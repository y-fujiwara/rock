(ns rock.token)

(defrecord ^{:private true} Token [tag lineNumber])
(defrecord ^{:private true} NumToken [tag lineNumber val])
(defrecord ^{:private true} IdToken [tag lineNumber text])
(defrecord ^{:private true} StrToken [tag lineNumber literal])

(defn new-token [line-number]
  (Token. ::Token line-number))

(defn new-num-token [line-number val]
  (NumToken. ::NumToken line-number val))

(defn new-id-token [line-number id]
  (IdToken. ::IdToken line-number id))

(defn new-str-token [line-number lit]
  (NumToken. ::StrToken line-number lit))

(derive ::NumToken ::Token)
(derive ::IdToken ::Token)
(derive ::StrToken ::Token)

(defonce EOF (Token. ::Token -1))
(defonce EOL "\\n")

(defmulti isIdentifier :tag)
(defmethod isIdentifier ::Token [_] false)
(defmethod isIdentifier ::IdToken [_] true)

(defmulti isNumber :tag)
(defmethod isNumber ::Token [_] false)
(defmethod isNumber ::NumToken [_] true)

(defmulti isString :tag)
(defmethod isString ::Token [_] false)
(defmethod isString ::StrToken [_] true)

(defmulti getNumber :tag)
(defmethod getNumber ::Token [_] nil)
(defmethod getNumber ::NumToken [num-tok] (.val num-tok))

(defmulti getText :tag)
(defmethod getText ::Token [_] "")
(defmethod getText ::NumToken [num-tok] (str (.val num-tok)))
(defmethod getText ::IdToken [id-tok] (.text id-tok))
(defmethod getText ::StrToken [str-tok] (.literal str-tok))
