(ns rock.lexer
  (:require [rock.token :as tok])
  (:import (java.io FileReader LineNumberReader))
  (:use [clojure.java.io :only (reader)]))

(declare read-line fill-queue?)

(defprotocol LexerMethods
  (read [this])
  (peek [this i]))

; private
; プロトコルにする意味が無い
(defrecord ^{:private true} Lexer [line-reader has-more queue]
  LexerMethods
  (read [this]
    (let [[l ret] (fill-queue? this 0)]
      (if ret
        ; immutableにするもっと良いほうがあるはず
        [(first (.queue l)) (assoc this :queue (rest (.queue l)))]
        [tok/EOF l])))

  (peek [this i]
    (if (fill-queue? this i)
      [(nth (.queue this) i) this]
      [tok/EOF this])))


(defonce ^{:private true} regex-pat
  #"\s*((//.*)|([0-9]+)|(\"(\\\"|\\\\|\\n|[^\"])*\")|[A-Z_a-z][A-Z_a-z0-9]*|==|<=|>=|&&|\|\||\p{Punct})?")

(defn- to-string-literal [s]
  (let [len (- (.length s) 1)]
    (loop [i 1 ret ""]
      (if (< i len)
        (let [c (.charAt s i)]
          (if (and (= c \\) (< (inc i) len))
            (let [c2 (.charAt (+ i 1))]
              (case c2
                (or (+ c2 \") (= c2 \\)) (recur (+ i 2) (str ret (.charAt s (+ i 2))))
                (= c2 \n) (recur (+ i 2) (str ret \n))
                :else (recur (+ i 1) (str ret c))))
            (recur (+ i 1) (str ret c))))
        ret))))

(defn- add-token [lex lineNo matcher]
  (if-let [m (.group matcher 1)]                            ; not space
    (if (nil? (.group matcher 2))                           ; not comment
      (cond
        ;; concatの挙動がdefrecordをばらしてconcatしてしまっている
        (.group matcher 3) (assoc
                             lex
                             :queue
                             (conj (.queue lex) (tok/new-num-token lineNo (read-string m))))
        (.group matcher 4) (assoc
                             lex
                             :queue
                             (conj (.queue lex) (tok/new-str-token lineNo (to-string-literal m))))
        :else (assoc
                lex
                :queue
                (conj (.queue lex) (tok/new-id-token lineNo m))))
      lex)
    lex))

(defn- match-token [lex line lineNo matcher]
  (let [init-pos 0
        end-pos (count line)]
    (loop [lex lex pos init-pos]
      (if (< pos end-pos)
        (do
          (.region matcher pos end-pos)
          (if (.lookingAt matcher)
            (recur (add-token lex lineNo matcher) (.end matcher))
            nil))
        (do
          (assoc lex :queue (conj (.queue lex) (tok/new-id-token lineNo tok/EOL))))))))

(defn- read-line [lex]
  (let [line (.readLine (.line-reader lex))]
    (if (nil? line)
      (Lexer. (.line-reader lex) false (.queue lex))
      (let [lineNo (.getLineNumber (.line-reader lex))
            matcher (.useAnchoringBounds (.useTransparentBounds (.matcher regex-pat line) true) false)]
        (match-token lex line lineNo matcher)))))

(defn- fill-queue? [lex i]
  (loop [lex lex]
    (if (>= i (count (.queue lex)))
      (if (.has-more lex)
        (recur (read-line lex))
        [nil false])
      [lex true])))

; 初期化関数
(defn new-lexer [reader]
  (Lexer. (LineNumberReader. reader) true []))
